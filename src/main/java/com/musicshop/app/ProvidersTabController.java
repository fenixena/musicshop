package com.musicshop.app;

import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;

class ProvidersTabController extends TabController {

    private static ProvidersManager tableManager = new ProvidersManager();

    ProvidersTabController(JTabbedPane tabsPanel, String tabName) {
        super(tabsPanel, tabName, tableManager);
    }

    @Override
    void createRows() {
        try {
            ResultSet resultSet = tableManager.getAll();
            while (resultSet.next()) {
                final String id = Integer.toString(resultSet.getInt("id"));
                String name = resultSet.getString("name");

                String[] data = {id, name};

                addRow(data, id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addRow(String[] data, String id) {
        new ProvidersEditableRow(rowsContainer, data, id, tableManager, this);
    }

    @Override
    void createHeaders() {
        String[] data = {"Id", "Name"};
        new HeadersRow(rowsContainer, data);
    }
}