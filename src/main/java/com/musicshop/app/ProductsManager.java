package com.musicshop.app;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProductsManager extends TableManager {

    @Override
    public ResultSet getAll() {
        String query = "SELECT * FROM products";
        ResultSet resultSet = null;

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }

    public void update(int id, String name, String IAN, int price, int providerId) {
        String query = "UPDATE products SET name = ?, IAN = ?, price = ?, provider = ? WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, IAN);
            preparedStatement.setInt(3, price);
            preparedStatement.setInt(4, providerId);
            preparedStatement.setInt(5, id);
            
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Update successful");
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Update error" + e);
        }
    }

    @Override
    public void add() {
        String query = "INSERT INTO products (name, IAN, price, provider) VALUES ('', '', 0, 1)";

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            statement.executeUpdate(query);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        String query = "DELETE FROM products WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}