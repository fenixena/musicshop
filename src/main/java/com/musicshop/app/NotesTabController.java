package com.musicshop.app;

import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;

class NotesTabController extends TabController {

    private static NotesManager tableManager = new NotesManager();

    NotesTabController(JTabbedPane tabsPanel, String tabName) {
        super(tabsPanel, tabName, tableManager);
    }

    @Override
    void createRows() {
        try {
            ResultSet resultSet = tableManager.getAll();
            while (resultSet.next()) {
                final String id = Integer.toString(resultSet.getInt("id"));
                String text = resultSet.getString("text");

                String[] data = {id, text};

                addRow(data, id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addRow(String[] data, String id) {
        new NotesEditableRow(rowsContainer, data, id, tableManager, this);
    }

    @Override
    void createHeaders() {
        String[] data = {"Id", "Text"};
        new HeadersRow(rowsContainer, data);
    }
}