package com.musicshop.app;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class NotesManager extends TableManager {

    @Override
    public ResultSet getAll() {
        String query = "SELECT * FROM notes WHERE noteOwnerId = ?";

        ResultSet resultSet = null;

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, UserManager.getId());

            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }
    

    public void update(int id, String text) {
        String query = "UPDATE Notes SET text = ? WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setString(1, text);
            preparedStatement.setInt(2, id);
            
            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Update successful");
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Update error" + e);
        }
    }

    @Override
    public void add() {
        String query = "INSERT INTO Notes (text, noteOwnerId) VALUES ('', ?)";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, UserManager.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        String query = "DELETE FROM Notes WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
