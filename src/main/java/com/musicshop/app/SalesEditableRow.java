package com.musicshop.app;

import javax.swing.*;
import java.awt.*;

public class SalesEditableRow extends EditableRow {

    private JTextField idTextField;
    private JTextField productIdTextField;
    private JTextField customerIdTextField;
    private JTextField amountTextField;
    private TabController tabController;
    private SalesManager tableManager;

    SalesEditableRow(JPanel rowsContainer,
                     String[] data,
                     String entryId,
                     SalesManager tableManager,
                     TabController tabController) {
        super(rowsContainer, data, entryId);
        this.tabController = tabController;
        this.tableManager = tableManager;
    }


    @Override
    protected void onUpdateButtonClick() {
        try {
            int productId = Integer.parseInt(productIdTextField.getText());
            int customerId = Integer.parseInt(customerIdTextField.getText());
            int amount = Integer.parseInt(amountTextField.getText());
            tableManager.update(Integer.parseInt(entryId), productId, customerId, amount);
        } catch (NumberFormatException event) {
            JOptionPane.showMessageDialog(null, "Product, customer and amount fields should be integer!");
        }
    }

    @Override
    protected void onDeleteButtonClick() {
        int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to remove this entry?", "Are you sure?", JOptionPane.YES_NO_OPTION);

        if (answer == 0) {
            tableManager.delete(Integer.parseInt(entryId));
            tabController.refreshEntries();
        }
    }

    @Override
    protected void initRowContainer() {
        super.initRowContainer();
        rowContainer.setLayout(new GridLayout(1, data.length, 0, 0));
    }

    @Override
    protected void createTextFields() {
        idTextField = createTextField(data[0]);
        idTextField.setEditable(false);
        productIdTextField = createTextField(data[1]);
        customerIdTextField = createTextField(data[2]);
        amountTextField = createTextField(data[3]);
    }
}
