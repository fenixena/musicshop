package com.musicshop.app;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class TabController {
    private final JTabbedPane tabsPanel;
    private String tabName;
    private TableManager tableManager;
    JPanel rowsContainer;

    TabController(JTabbedPane tabsPanel, String tabName, TableManager tableManager) {
        this.tabsPanel = tabsPanel;
        this.tabName = tabName;
        this.tableManager = tableManager;
        createTab();
    }

    private void createTab() {
        JPanel productsTab = new JPanel();

        tabsPanel.add(productsTab, tabName);

        rowsContainer = new JPanel();
        rowsContainer.setLayout(new GridLayout(0, 1, 0, 10));

        JScrollPane scrollPane = new JScrollPane(rowsContainer);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setPreferredSize(new Dimension(800, 570));

        productsTab.add(scrollPane);
        populateTab();
    }

    private void populateTab() {
        createHeaders();
        createRows();
        addNewButton();
    }

    void refreshEntries() {
        rowsContainer.removeAll();
        populateTab();
    }

    private void addNewButton() {
        JButton addNewButton = new JButton("Add");

        addNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onAddButtonClick();
            }
        });

        rowsContainer.add(addNewButton);
    }
    
    private void onAddButtonClick() {
        tableManager.add();
        refreshEntries();
    }
    
    void createRows() {
        
    }

    void createHeaders() {

    }
}
