package com.musicshop.app;

import javax.swing.*;
import java.awt.event.WindowEvent;

public class FrameManager {
    private static JFrame previousFrame;
    private static Boolean isFrameInitialized = false;

    public FrameManager(String frameName, JPanel panel) {
        setNewFrame(frameName, panel);
    }
    
    public static void setNewFrame(String frameName, JPanel panel) {
        JFrame newFrame = new JFrame(frameName);
        newFrame.setContentPane(panel);
        setFrameVisible(newFrame);

        if (isFrameInitialized) {
            previousFrame.setVisible(false);
            previousFrame.dispose();
        }
        
        previousFrame = newFrame;
        isFrameInitialized = true;
    }
    
    private static void setFrameVisible(JFrame frame) {
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static JFrame getCurrentFrame() {
        return previousFrame;
    }
}