package com.musicshop.app;

public class Main {
    
    public static void main(String[] args) {
        FrameManager frameManager = new FrameManager("MusicShop", new MusicShop().mainPanel);
    }

    public static void authPerformed(String loggedUserName) {
        UserManager.init(loggedUserName);
        FrameManager.setNewFrame("MainForm", new MainForm().mainPanel);
        TimeManager timeManager = new TimeManager();
    }
}