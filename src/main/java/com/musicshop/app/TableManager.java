package com.musicshop.app;

import java.sql.ResultSet;

abstract public class TableManager {

    public abstract ResultSet getAll();

    public abstract void add();

    public abstract void delete(int id);
}