package com.musicshop.app;

import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;

class CustomersTabController extends TabController {

    private static CustomersManager tableManager = new CustomersManager();

    CustomersTabController(JTabbedPane tabsPanel, String tabName) {
        super(tabsPanel, tabName, tableManager);
    }

    @Override
    void createRows() {
        try {
            ResultSet resultSet = tableManager.getAll();
            while (resultSet.next()) {
                final String id = Integer.toString(resultSet.getInt("id"));
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");

                String[] data = {id, name, address};

                addRow(data, id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addRow(String[] data, String id) {
        new CustomersEditableRow(rowsContainer, data, id, tableManager, this);
    }

    @Override
    void createHeaders() {
        String[] data = {"Id", "Name", "Address"};
        new HeadersRow(rowsContainer, data);
    }
}