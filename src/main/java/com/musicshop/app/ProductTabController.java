package com.musicshop.app;

import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;

class ProductTabController extends TabController {
    
    private static ProductsManager productsManager = new ProductsManager();
    
    ProductTabController(JTabbedPane tabsPanel, String tabName) {
        super(tabsPanel, tabName, productsManager);
    }
    
    @Override
    void createRows() {
        try {
            ResultSet resultSet = productsManager.getAll();
            while (resultSet.next()) {
                final String id = Integer.toString(resultSet.getInt("id"));
                String name = resultSet.getString("name");
                String IAN = resultSet.getString("IAN");
                int price = resultSet.getInt("price");
                int providerId = resultSet.getInt("provider");

                String[] data = {id, name, IAN, Integer.toString(price), Integer.toString(providerId)};
                
                addRow(data, id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    private void addRow(String[] data, String id) {
        new ProductsEditableRow(rowsContainer, data, id, productsManager, this);
    }

    @Override
    void createHeaders() {
        String[] data = {"Id", "Name", "IAN", "Price", "Provider ID"};
        new HeadersRow(rowsContainer, data);
    }
}