package com.musicshop.app;

import javax.swing.*;
import java.awt.*;

public class ProductsEditableRow extends EditableRow {

    private JTextField nameTextField;
    private JTextField IANTextField;
    private JTextField priceTextField;
    private JTextField providerTextField;
    private JTextField idTextField;
    private TabController tabController;
    private ProductsManager tableManager;

    ProductsEditableRow(JPanel rowsContainer,
                        String[] data,
                        String entryId,
                        ProductsManager tableManager,
                        TabController tabController) {
        super(rowsContainer, data, entryId);
        this.tabController = tabController;
        this.tableManager = tableManager;
    }


    @Override
    protected void onUpdateButtonClick() {
        try {
            int price = Integer.parseInt(priceTextField.getText());
            int providerId = Integer.parseInt(providerTextField.getText());
            tableManager.update(Integer.parseInt(entryId), nameTextField.getText(), IANTextField.getText(), price, providerId);
        } catch (NumberFormatException event) {
            JOptionPane.showMessageDialog(null, "Price and provider id should be integer!");
        }
    }

    @Override
    protected void onDeleteButtonClick() {
        int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to remove this entry?", "Are you sure?", JOptionPane.YES_NO_OPTION);

        if (answer == 0) {
            tableManager.delete(Integer.parseInt(entryId));
            tabController.refreshEntries();
        }
    }

    @Override
    protected void initRowContainer() {
        super.initRowContainer();
        rowContainer.setLayout(new GridLayout(1, data.length, 0, 0));
    }

    @Override
    protected void createTextFields() {
        idTextField = createTextField(data[0]);
        idTextField.setEditable(false);
        nameTextField = createTextField(data[1]);
        IANTextField = createTextField(data[2]);
        priceTextField = createTextField(data[3]);
        providerTextField = createTextField(data[4]);
    }
}
