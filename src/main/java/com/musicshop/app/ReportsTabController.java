package com.musicshop.app;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfWriter;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

class ReportsTabController {

    private JPanel reportsTab;
    private JTabbedPane tabsPanel;
    private JPanel contentContainer;

    ReportsTabController(JTabbedPane tabsPanel) {
        this.tabsPanel = tabsPanel;
        createTab();
    }

    private void createTab() {
        reportsTab = new JPanel();

        tabsPanel.add(reportsTab, "Reports");

        contentContainer = new JPanel();
        contentContainer.setPreferredSize(new Dimension(800, 570));

        reportsTab.add(contentContainer);

        addSalesReportButton();
    }

    private void addSalesReportButton() {
        JButton salesReportButton = new JButton("See sales report");
        contentContainer.add(salesReportButton);

        salesReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Document document = new Document();

                ResultSet resultSet = DBManager.getSalesReport();

                try {
                    try {
                        PdfWriter.getInstance(document, new FileOutputStream("report.pdf"));
                    } catch (FileNotFoundException exception) {
                        System.out.println("Error" + exception);
                    }

                    document.open();
                    Font font = FontFactory.getFont(FontFactory.COURIER, 14, BaseColor.BLACK);

                    try {
                        while (resultSet.next()) {
                            int amount = resultSet.getInt(4);
                            String customerName = resultSet.getString(7);
                            String customerAddress = resultSet.getString(8);
                            String productName = resultSet.getString(10);
                            String productIAN = resultSet.getString(11);
                            int productPrice = resultSet.getInt(12);

                            document.add(new Paragraph("Customer: " + customerName + ", " + customerAddress));
                            document.add(new Paragraph("Bought: " + productName + ". IAN: " + productIAN));
                            document.add(new Paragraph("Amount: " + Integer.toString(amount)));
                            document.add(new Paragraph("Total sum: $" + Integer.toString(productPrice * amount)));
                            document.add( Chunk.NEWLINE );
                        }
                    } catch (SQLException sqlException) {
                        System.out.println("Error" + sqlException);
                    }


                    document.close();

                    try {
                        Desktop.getDesktop().open(new File("report.pdf"));
                    } catch (IOException ioException) {
                        System.out.println("Error" + ioException);
                    }

                } catch (DocumentException event) {
                    System.out.println("Error" + event);
                }
            }
        });
    }
}