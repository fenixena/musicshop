package com.musicshop.app;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;


public class TimeManager {
    private Timer timer;
    private Integer delayTime = 1000;
    private Boolean isTimerPaused = true;
    private PreparedStatement preparedStatement;

    public TimeManager() {
        final String query = "UPDATE users SET time = time + ? WHERE name = ?";

        try {
            preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, delayTime);
            preparedStatement.setString(2, UserManager.getUsername());

            initWindowListeners();
            startTimer();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initWindowListeners() {
        JFrame currentFrame = FrameManager.getCurrentFrame();

        currentFrame.addWindowListener(new WindowAdapter() {
            public void windowActivated(WindowEvent e) {
                System.out.println("windowActivated");
                if (isTimerPaused) {
                    startTimer();
                }
            }

            public void windowDeactivated(WindowEvent e) {
                System.out.println("windowDeactivated");
                if (!isTimerPaused) {
                    stopTimer();
                }
            }
        });
    }

    private void stopTimer() {
        timer.cancel();
        isTimerPaused = true;
        System.out.println("Timer paused");
    }

    private void startTimer() {
        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    preparedStatement.executeUpdate();
                    System.out.println("+ 1s to spent time in application.");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }, delayTime, delayTime);

        isTimerPaused = false;
        System.out.println("Timer started");
    }
}