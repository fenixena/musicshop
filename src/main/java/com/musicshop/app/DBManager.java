package com.musicshop.app;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager {

    static ResultSet getSalesReport() {
        if (UserManager.getRole().equals(Constants.ADMIN_ROLE)) {
            return getFullSalesReport();
        } else {
            return getSalesReportByUserId();
        }
    }

    private static ResultSet getFullSalesReport() {
        String query = "SELECT * FROM sales INNER JOIN customers ON sales.customer=customers.id INNER JOIN products ON sales.product=products.id";

        ResultSet resultSet = null;

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }

    private static ResultSet getSalesReportByUserId() {
        String query = "SELECT * FROM sales INNER JOIN customers ON sales.customer=customers.id INNER JOIN products ON sales.product=products.id WHERE owner = ?";

        ResultSet resultSet = null;

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, UserManager.getId());

            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }
}