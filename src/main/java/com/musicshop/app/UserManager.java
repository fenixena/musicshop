package com.musicshop.app;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserManager {
    private static String userName;
    private static String userRole;
    private static Long timeSpent;
    private static int userId;

    public static void init(String loggedUserName) {
        userName = loggedUserName;
        getUserData();
    }

    private static void getUserData() {
        String GetCustomUserDataQuery = "SELECT * FROM users WHERE name = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(GetCustomUserDataQuery);
            preparedStatement.setString(1, userName);
            ResultSet customUserData = preparedStatement.executeQuery();

            customUserData.next();
            userId = customUserData.getInt(1);
            userRole = customUserData.getString(3);
            timeSpent = customUserData.getLong(4);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getUsername() {
        return userName;
    }

    public static String getRole() {
        return userRole;
    }

    public static int getId() {
        return userId;
    }

    public static Long getTimeSpent() {
        getUserData();
        return timeSpent;
    }

    public static Boolean createNewUser(String name, String password, String role) {
        Boolean success = false;

        String SQLUserQuery = "CREATE USER ?@localhost IDENTIFIED BY ?";
        String privilegesQuery = "GRANT ALL PRIVILEGES ON * . * TO ?@localhost";
        String usersTableQuery = "INSERT INTO users (name, role, time) VALUES (?, ?, ?)";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(SQLUserQuery);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);
            preparedStatement.executeUpdate();

            PreparedStatement privilegesStatement = DBConnection.getConnection().prepareStatement(privilegesQuery);
            privilegesStatement.setString(1, name);
            privilegesStatement.executeUpdate();

            PreparedStatement usersTableStatement = DBConnection.getConnection().prepareStatement(usersTableQuery);
            usersTableStatement.setString(1, name);
            usersTableStatement.setString(2, role);
            usersTableStatement.setInt(3, 0);
            usersTableStatement.executeUpdate();

            success = true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error creating user. Probably this name already exists. " + e);
        }

        return success;
    }
    
    public static ResultSet getAllUsers() {
        String GetAllUsersQuery = "SELECT * FROM users";
        ResultSet allUsersResultSet = null;
                
        try {
            Statement statement = DBConnection.getConnection().createStatement();
            allUsersResultSet = statement.executeQuery(GetAllUsersQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return allUsersResultSet;
    }
}