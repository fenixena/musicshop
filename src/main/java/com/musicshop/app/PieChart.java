package com.musicshop.app;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import javax.swing.*;

public class PieChart extends JFrame {
    
    private static DefaultPieDataset dataset;

    public PieChart(String title, DefaultPieDataset _dataset ) {
        super( title );
        dataset = _dataset;
        setContentPane(createChartPanel());
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    private static JFreeChart createChart(PieDataset dataset ) {
        return ChartFactory.createPieChart(
                "Time Spent",   // chart title
                dataset,          // data
                true,             // include legend
                true,
                false);
    }

    public static JPanel createChartPanel( ) {
        JFreeChart chart = createChart(dataset);
        return new ChartPanel( chart );
    }
}