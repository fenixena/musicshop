package com.musicshop.app;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CustomersManager extends TableManager {

    @Override
    public ResultSet getAll() {
        String query = "SELECT * FROM customers";
        ResultSet resultSet = null;

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }
    

    public void update(int id, String name, String address) {
        String query = "UPDATE customers SET name = ?, address = ? WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, address);
            preparedStatement.setInt(3, id);
            
            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Update successful");
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Update error" + e);
        }
    }

    @Override
    public void add() {
        String query = "INSERT INTO customers (name, address) VALUES ('', '')";

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            statement.executeUpdate(query);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        String query = "DELETE FROM customers WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
