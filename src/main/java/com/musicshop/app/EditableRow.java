package com.musicshop.app;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditableRow {

    String entryId;
    JPanel rowContainer;
    private JPanel rowsContainer;
    String[] data;

    EditableRow(JPanel rowsContainer,
                String[] data,
                String entryId) {

        this.rowsContainer = rowsContainer;
        this.data = data;
        this.entryId = entryId;

        initRowContainer();
        createTextFields();
        addUpdateButton();
        addDeleteButton();
    }

    protected void initRowContainer() {
        rowContainer = new JPanel();
        rowContainer.setPreferredSize(new Dimension(800, 40));
        rowsContainer.add(rowContainer);
    }

    private void addUpdateButton() {
        JButton updateButton = new JButton("Update");
        updateButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onUpdateButtonClick();
            }
        });
        rowContainer.add(updateButton);
    }

    private void addDeleteButton() {
        JButton deleteButton = new JButton("Delete");
        deleteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onDeleteButtonClick();
            }
        });

        rowContainer.add(deleteButton);
    }

    protected void createTextFields() {

    }

    JTextField createTextField(String name) {
        JTextField textField = new JTextField(name);
        rowContainer.add(textField);
        return textField;
    }

    protected void onUpdateButtonClick() {

    }

    protected void onDeleteButtonClick() {

    }
}