package com.musicshop.app;

import javax.swing.*;
import java.awt.*;

public class NotesEditableRow extends EditableRow {

    private JTextField idTextField;
    private JTextField textField;
    private TabController tabController;
    private NotesManager tableManager;

    NotesEditableRow(JPanel rowsContainer,
                     String[] data,
                     String entryId,
                     NotesManager tableManager,
                     TabController tabController) {
        super(rowsContainer, data, entryId);
        this.tabController = tabController;
        this.tableManager = tableManager;
    }


    @Override
    protected void onUpdateButtonClick() {
        tableManager.update(Integer.parseInt(entryId), textField.getText());
    }

    @Override
    protected void onDeleteButtonClick() {
        int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to remove this entry?", "Are you sure?", JOptionPane.YES_NO_OPTION);

        if (answer == 0) {
            tableManager.delete(Integer.parseInt(entryId));
            tabController.refreshEntries();
        }
    }

    @Override
    protected void initRowContainer() {
        super.initRowContainer();
        rowContainer.setLayout(new GridLayout(1, data.length, 0, 0));
    }

    @Override
    protected void createTextFields() {
        idTextField = createTextField(data[0]);
        idTextField.setEditable(false);
        textField = createTextField(data[1]);
    }
}
