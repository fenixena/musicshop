package com.musicshop.app;

import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.Provider;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MainForm extends JFrame {
    public JPanel mainPanel;
    private JTabbedPane tabsPanel;
    private JLabel timeUserName;
    private JTextField loginField;
    private JButton createNewUserButton;
    private JPanel newUserPanel;
    private JButton submitUser;
    private JComboBox roleDropdown;
    private JPasswordField passwordField;
    private JPasswordField repeatPasswordField;
    private JPanel userTab;
    private JPanel timeTab;
    private JLabel timeSpentLabel;
    private JButton refreshTimeButton;
    private JButton allUsersTimeButton;
    private JPanel productsPanel;
    private JLabel Name;
    private JPanel productsTab = null;

    public MainForm() {
        configureFormBasedOnRole();
        fillFormWithData();

        submitUser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = loginField.getText();
                String password = String.valueOf(passwordField.getPassword());
                String repeatPassword = String.valueOf(repeatPasswordField.getPassword());

                if (name.equals("") || password.equals("") || repeatPassword.equals("")) {
                    JOptionPane.showMessageDialog(null, "All fields should be filled.");
                } else if (!password.equals(repeatPassword)) {
                    JOptionPane.showMessageDialog(null, "Passwords don't match");
                } else if (name.contains(" ")) {
                    JOptionPane.showMessageDialog(null, "User name shouldn't have spaces");
                } else {
                    if (UserManager.createNewUser(name, password, roleDropdown.getSelectedItem().toString())) {
                        loginField.setText("");
                        passwordField.setText("");
                        repeatPasswordField.setText("");
                        JOptionPane.showMessageDialog(null, "User has been successfully created.");
                    }
                }
            }
        });

        allUsersTimeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                initTimeChart();
            }
        });

        refreshTimeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setTimeSpentLabel();
            }
        });
    }

    private void configureFormBasedOnRole() {
        if (UserManager.getRole().equals(Constants.USER_ROLE)) {
            tabsPanel.remove(userTab);
        }
    }

    private void fillFormWithData() {
        timeUserName.setText(UserManager.getUsername());
        setTimeSpentLabel();
        new ProductTabController(tabsPanel, "Products");
        new ProvidersTabController(tabsPanel, "Providers");
        new CustomersTabController(tabsPanel, "Customers");
        new SalesTabController(tabsPanel, "Sales");
        new NotesTabController(tabsPanel, "Notes");
        new ReportsTabController(tabsPanel);
    }

    private void setTimeSpentLabel() {
        Long totalSeconds = UserManager.getTimeSpent() / 1000;
        Long minutesSpent = totalSeconds / 60;
        Long secondsSpent = totalSeconds % 60;
        timeSpentLabel.setText("Time spent: " + minutesSpent + " minutes " + secondsSpent + " seconds");
    }

    private void initTimeChart() {
        DefaultPieDataset dataset = new DefaultPieDataset();

        try {
            ResultSet allUsers = UserManager.getAllUsers();

            while (allUsers.next()) {
                String userName = allUsers.getString("name");
                int timeSpent = allUsers.getInt("time");
                dataset.setValue(userName, timeSpent);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        PieChart pieChart = new PieChart("Time Spent", dataset);
        pieChart.setSize(560, 367);
        RefineryUtilities.centerFrameOnScreen(pieChart);
        pieChart.setVisible(true);
    }
}
