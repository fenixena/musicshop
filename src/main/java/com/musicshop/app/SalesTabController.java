package com.musicshop.app;

import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;

class SalesTabController extends TabController {

    private static SalesManager tableManager = new SalesManager();

    SalesTabController(JTabbedPane tabsPanel, String tabName) {
        super(tabsPanel, tabName, tableManager);
    }

    @Override
    void createRows() {
        try {
            ResultSet resultSet = tableManager.getAll();
            while (resultSet.next()) {
                final String id = Integer.toString(resultSet.getInt("id"));
                String customer = Integer.toString(resultSet.getInt("customer"));
                String product = Integer.toString(resultSet.getInt ("product"));
                String amount = Integer.toString(resultSet.getInt("amount"));

                String[] data = {id, customer, product, amount};

                addRow(data, id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addRow(String[] data, String id) {
        new SalesEditableRow(rowsContainer, data, id, tableManager, this);
    }

    @Override
    void createHeaders() {
        String[] data = {"Id", "Customer id", "Product id", "Amount"};
        new HeadersRow(rowsContainer, data);
    }
}