package com.musicshop.app;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SalesManager extends TableManager {

    @Override
    public ResultSet getAll() {
        if (UserManager.getRole().equals(Constants.ADMIN_ROLE)) {
            return getAllEntries();
        } else {
            return getEntriesByOwner();
        }
    }

    private ResultSet getAllEntries() {
        String query = "SELECT * FROM sales";

        ResultSet resultSet = null;

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }

    private ResultSet getEntriesByOwner() {
        String query = "SELECT * FROM sales WHERE owner = ?";

        ResultSet resultSet = null;

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, UserManager.getId());

            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }
    

    public void update(int id, int customerId, int productId, int amount) {
        String query = "UPDATE sales SET customer = ?, product = ?, amount = ? WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, customerId);
            preparedStatement.setInt(2, productId);
            preparedStatement.setInt(3, amount);
            preparedStatement.setInt(4, id);
            
            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Update successful");
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Update error" + e);
        }
    }

    @Override
    public void add() {
        String query = "INSERT INTO sales (customer, product, amount, owner) VALUES (1, 1, 0, ?)";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);

            preparedStatement.setInt(1, UserManager.getId());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        String query = "DELETE FROM sales WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
