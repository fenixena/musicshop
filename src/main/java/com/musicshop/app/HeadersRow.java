package com.musicshop.app;

import javax.swing.*;
import java.awt.*;

class HeadersRow {

    HeadersRow(JPanel rowsContainer, String[] data) {
        JPanel rowContainer = new JPanel();
        rowContainer.setLayout(new GridLayout(1, data.length));

        rowsContainer.add(rowContainer);

        for (String header : data) {
            JLabel headerLabel = new JLabel(header);
            headerLabel.setHorizontalAlignment(JLabel.CENTER);
            rowContainer.add(headerLabel);
        }

        JLabel updateLabel = new JLabel("Update");
        updateLabel.setHorizontalAlignment(JLabel.CENTER);
        rowContainer.add(updateLabel);

        JLabel deleteLabel = new JLabel("Delete");
        deleteLabel.setHorizontalAlignment(JLabel.CENTER);
        rowContainer.add(deleteLabel);
    }
}