package com.musicshop.app;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProvidersManager extends TableManager {

    @Override
    public ResultSet getAll() {
        String query = "SELECT * FROM providers";
        ResultSet resultSet = null;

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }
    

    public void update(int id, String name) {
        String query = "UPDATE providers SET name = ? WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, id);
            
            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Update successful");
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Update error" + e);
        }
    }

    @Override
    public void add() {
        String query = "INSERT INTO providers (name) VALUES ('')";

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            statement.executeUpdate(query);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        String query = "DELETE FROM providers WHERE id = ?";

        try {
            PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
