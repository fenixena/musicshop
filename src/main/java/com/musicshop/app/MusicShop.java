package com.musicshop.app;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MusicShop {
    private JPasswordField passwordPasswordField;
    private JTextField loginTextField;
    private JButton loginButton;
    public JPanel mainPanel;

    public MusicShop() {

        loginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String login = loginTextField.getText();
                String password = String.valueOf(passwordPasswordField.getPassword());

                System.out.println(login + " " + password);

                if (!login.equals("") && !password.equals("")) {
                     if (DBConnection.connect(login, password)) {
                         Main.authPerformed(login);
                     } else {
                         JOptionPane.showMessageDialog(null, "Wrong login or password provided.");
                     }
                } else {
                    JOptionPane.showMessageDialog(null, "Login and password fields should not be empty!");
                }
            }
        });
    }
}