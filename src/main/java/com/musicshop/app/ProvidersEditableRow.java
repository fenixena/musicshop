package com.musicshop.app;

import javax.swing.*;
import java.awt.*;

public class ProvidersEditableRow extends EditableRow {

    private JTextField idTextField;
    private JTextField nameTextField;
    private TabController tabController;
    private ProvidersManager tableManager;

    ProvidersEditableRow(JPanel rowsContainer,
                         String[] data,
                         String entryId,
                         ProvidersManager tableManager,
                         TabController tabController) {
        super(rowsContainer, data, entryId);
        this.tabController = tabController;
        this.tableManager = tableManager;
    }


    @Override
    protected void onUpdateButtonClick() {
        tableManager.update(Integer.parseInt(entryId), nameTextField.getText());
    }

    @Override
    protected void onDeleteButtonClick() {
        int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to remove this entry?", "Are you sure?", JOptionPane.YES_NO_OPTION);

        if (answer == 0) {
            tableManager.delete(Integer.parseInt(entryId));
            tabController.refreshEntries();
        }
    }

    @Override
    protected void initRowContainer() {
        super.initRowContainer();
        rowContainer.setLayout(new GridLayout(1, data.length, 0, 0));
    }

    @Override
    protected void createTextFields() {
        idTextField = createTextField(data[0]);
        idTextField.setEditable(false);
        nameTextField = createTextField(data[1]);
    }
}
