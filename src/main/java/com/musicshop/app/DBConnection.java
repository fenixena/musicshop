package com.musicshop.app;

import com.mysql.cj.jdbc.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnection {

    private static Connection connection;

    public static boolean connect(String username, String password) {
        String URL = "jdbc:mysql://localhost:3306/musicshop?useSSL=false&allowPublicKeyRetrieval=true";
        Boolean isConnected = false;

        try {
            Driver driver = new Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            System.err.println("Cannot create driver");
        }

        try {
            connection = DriverManager.getConnection(URL, username, password);
            
            if (!connection.isClosed()) {
                System.out.println("Connection established");
                isConnected = true;
            }
        }   catch (SQLException e) {
            System.err.println("Connection isn't established. " + e);
        }
        
        return isConnected;
    }

    public static Connection getConnection() {
        return connection;
    }
}