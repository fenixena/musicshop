-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: musicshop
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'John Smith','743-701 E 19th Ave Denver, CO 80203 USA '),(2,'Todd Brook','722-1 15th Ave  NY, CO 19151  USA '),(3,'Matt Kelly','650 15th St, San Francisco, CO 80202, USA'),(4,'Edwin Amirian','1600 E 18th Ave, New York, CO 80218, USA'),(5,'David Spellman','2550 Welton St, Las Vegas, CO 80205, USA'),(6,'Damein Lawson','3041 Zuni St, Los Angeles, CO 80211, USA'),(7,'Tiffany Steffen','3333 Regis Blvd, Los Angeles, CO 80221, USA'),(8,'Fran Wilkinson','5801 Brighton Blvd, Commerce City, CO 80022, USA'),(9,'Matt Yankelovich','Arvada, CO 80003, United States'),(10,'Chris Lim','7150 Montview Blvd # 1, Hudson, CO 80220, USA'),(11,'Burns Denise','10400 E 29th Dr, Concord, CO 80238, USA'),(12,'Robert Folk','2831 Hanover St, Denver, CO 80238, USA');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `IAN` varchar(45) NOT NULL,
  `price` int(11) NOT NULL,
  `provider` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IAN_UNIQUE` (`IAN`),
  KEY `provider_idx` (`provider`),
  CONSTRAINT `provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Acoustic Guitar Yamaha C40','AGYC40',115,NULL),(2,'Guitar Amp Blackstar HT-5RH MkII','HT-5RHMkII',394,NULL),(3,'Electric Guitar Gibson Les Paul Custom EB GH','EGGLPCEBGH',3222,NULL),(4,'Acoustic Drums Ludwig Classic Maple Fab 22','ADLCMF22BO',2819,NULL),(5,'Electronic Drums Roland TD-17KVX E-Drum Set','EDRTD17KVX',1505,NULL),(6,'Drum Cymbal Sabian 06\" AAX Splash','DCS06AAXS',93,NULL),(7,'Masterwork 06\" Custom Splash','M06CS',61,NULL),(8,'Keyboard Thomann SP-5600','KTSP5600',345,NULL),(11,'Cello Stentor SR1108 Cello Student II 4/4','CSR1108CSII44',575,NULL),(12,'Acoustic Piano Kawai K-300 ATX 3 E/P SL','APK300ATX3EPSL',7799,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `providers`
--

DROP TABLE IF EXISTS `providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `providers`
--

LOCK TABLES `providers` WRITE;
/*!40000 ALTER TABLE `providers` DISABLE KEYS */;
INSERT INTO `providers` VALUES (1,'Music LTD CO.'),(2,'GuitarCenter'),(3,'Halpins'),(4,'Dean Guitars'),(5,'Yamaha'),(6,'AnyInstrument inc.'),(7,'Fender'),(8,'Pearl Drums'),(9,'Roland Company'),(10,'Alesis'),(11,'Millenium');
/*!40000 ALTER TABLE `providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_idx` (`product`),
  KEY `customer_idx` (`customer`),
  KEY `owner_idx` (`owner`),
  CONSTRAINT `customer` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `owner` FOREIGN KEY (`owner`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `product` FOREIGN KEY (`product`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (1,1,1,5,1),(4,3,5,2,1),(5,2,7,9,4),(6,4,2,1,5),(7,9,8,6,6),(8,2,7,2,1),(9,3,2,4,5),(10,12,3,1,6),(11,11,4,1,11),(12,5,6,2,11),(13,6,2,1,7),(14,7,12,2,7),(15,1,3,1,NULL),(16,1,4,2,NULL),(17,2,5,6,NULL),(18,1,4,7,NULL),(19,5,7,4,NULL),(20,5,5,5,NULL),(21,2,8,7,NULL),(22,1,12,6,17),(23,4,2,3,17),(24,8,11,1,17),(25,1,5,1,17),(26,1,3,1,17);
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  `time` bigint(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'root','Admin',2860000),(4,'John','User',245000),(5,'Dima','User',110000),(6,'Andrew','User',300000),(7,'Joe','User',150000),(11,'Admin','Admin',25000),(17,'Dod','User',68000);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-04 15:38:22
